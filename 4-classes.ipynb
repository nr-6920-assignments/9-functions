{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Classes\n",
    "\n",
    "This is going to be the shortest introduction to classes in history. Mostly I just want you to have a vague idea of what they are.\n",
    "\n",
    "You may or may not remember the discussion about classes back at the beginning of the semester. You know the objects that you've been working with that are more complicated than strings or numbers? Spatial reference objects and raster objects are two examples. Well, those objects are instances of *classes*. A class is what defines the properties and methods an object will have.\n",
    "\n",
    "Here's a really simple class called `Person`. All it can do is say hello."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Person(object):\n",
    "    \n",
    "    def say_hello(self):\n",
    "        print('Hello!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first line of that example is the part that names the class. The stuff in parentheses (`object`) tells the class what to inherit from. That's a discussion that I don't really want to get into right now, but it's really powerful. For the purposes of this example, we're just saying that a `Person` has the same properties and methods that a generic Python `object` has, along with the extra properties and methods that we're going to give it.\n",
    "\n",
    "The second part of the code defines a method that lives on the class. This is very similar to a function, except that you can't call it by itself (for example, just saying `say_hello()` won't work, because it needs to be attached to a `Person`). The `self` parameter to this method is the object itself. This allows your code to access other parts of the class, but the user doesn't need to provide it when calling the method. This parameter is required when defining a method, whether or not your code uses it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Okay, so let's create a `Person` and have it say hello."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bob = Person()\n",
    "bob.say_hello()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Say we want our `Person` to know its name. Let's add a method that allows the user to set the object's name. We'll also modify the `say_hello()` method so that it uses that name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Person(object):\n",
    "\n",
    "    def set_name(self, name):\n",
    "        self.my_name = name\n",
    "        \n",
    "    def say_hello(self):\n",
    "        print(f'Hello from {self.my_name}!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `set_name()` method wants the required `self` parameter, and a second one called `name`. It then saves that name as a property on itself (`self.my_name`). Then the `say_hello()` function uses `self.my_name` when printing a message. The `self` variable is only available to code inside the class.\n",
    "\n",
    "Now let's create another `Person` based on this new class definition. We can tell it that its name is 'Joe' and then have it print a message."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "joe = Person()\n",
    "joe.set_name('Joe')\n",
    "joe.say_hello()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also set the name when you first create the `Person`. You still need to write a method that does it, but the user doesn't need to call an extra method in order to set the name. In order to do this, you create what's called a *constructor* that constructs the new object. This method **must** be named `__init__()`, so let's change the name of `set_name()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Person(object):\n",
    "\n",
    "    def __init__(self, name):\n",
    "        self.my_name = name\n",
    "        \n",
    "    def say_hello(self):\n",
    "        print(f'Hello from {self.my_name}!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the user needs to provide a name when creating the `Person`. The code will crash if they don't, because `name` doesn't have a default value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mike = Person('Mike')\n",
    "mike.say_hello()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pretty cool, huh. A class definition can have as many properties and methods as you want. Let's add an age and a couple more methods to the `Person` class. The default age will be 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Person(object):\n",
    "\n",
    "    def __init__(self, name, age=0):\n",
    "        self.my_name = name\n",
    "        self.my_age = age\n",
    "        \n",
    "    def say_hello(self):\n",
    "        print(f'Hello from {self.my_name}!')\n",
    "        \n",
    "    def age(self):\n",
    "        print(f'I am {self.my_age} years old.')\n",
    "        \n",
    "    def have_birthday(self):\n",
    "        print(\"I'm having a birthday!\")\n",
    "        self.my_age = self.my_age + 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can create a new `Person` that is 10 years old and then make them have a birthday."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sue = Person('Sue', 10)\n",
    "sue.say_hello()\n",
    "\n",
    "# Print the current age\n",
    "sue.age()\n",
    "\n",
    "# Have a birthday and print the new age\n",
    "sue.have_birthday()\n",
    "sue.age()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could keep doing that forever and Sue would always know her age until we restarted Python or overwrite the `sue` variable with a new `Person` or something else."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sue.have_birthday()\n",
    "sue.age()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Classes are a great way to keep related data and methods together in one place. And since an object knows information about itself, the user doesn't need to remember it. We didn't need to create a variable for Sue's age because she can remember it for us. Handy!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
