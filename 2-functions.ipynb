{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions\n",
    "\n",
    "You've used a lot of functions to do do various things each week, and now you're going to see how to write your own. It's not hard, and it's very useful if you need to use the same bits of code over and over. For example, it makes long scripts easier to understand if you break your code into multiple chunks, each of which does a specific thing. These chunks make up your functions.\n",
    "\n",
    "Know how I've been having you put variables at the top of your notebooks and making you use them throughout your code instead of the actual values contained in the variables? That'll make even more sense when you learn how to write functions. The entire point of them is that they are reusable bits of code that can work on different inputs, which is exactly how I've been trying to get you to set up your notebooks. Functions are much more versatile than a notebook, however, because you can put them in custom modules and import and use them in other Python code, including notebooks.\n",
    "\n",
    "To write a function, start off with the `def` keyword, then the name of your function and a set of parentheses containing the parameters that your function will work with. Follow that with a colon and indent the body of the function underneath. The function ends when the indentation stops.\n",
    "\n",
    "Here's an example of an incredibly simple function called `say_hello()` that doesn't do anything but print a predefined message. The user doesn't have to provide any input at all (because the parentheses after `say_hello` are empty)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def say_hello():\n",
    "    print('Hello world!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That *defined* your function, but didn't run its code, which is why nothing was printed. Once you've defined your function, you can call it like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "say_hello()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 1\n",
    "\n",
    "Create a function called `multiply()` that multiplies 2 * 2 and prints out the answer. There's no reason to convert anything to strings-- just do the math and print the result. \n",
    "\n",
    "Then call your function. It should print out 4."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parameters\n",
    "\n",
    "Now let's add a parameter called `name` to the `say_hello()` function. Parameters are the values that the user provides, so they can be different every time the function is called. They're just like variables and you can call them whatever you want, but you should give each one a descriptive name so that you know what it is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def say_hello(name):\n",
    "    print(f'Hello {name}!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now your function creates and prints a string that includes the name that the user provides."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "say_hello('Bob')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also provide the parameter name when you call the function, like I've tried to do for most of the arcpy geoprocessing tools:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "say_hello(name='Joe')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 2\n",
    "\n",
    "Create a new function called `multiply()` that takes one number as a parameter. Have it multiply that number by itself and print out the answer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "This'll call your function. If it doesn't print out 9, you've got a problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "multiply(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scope\n",
    "\n",
    "*Scope* refers to where in the script a variable is available for use. Read on to see examples of what I mean.\n",
    "\n",
    "If you want to pass a variable to a function, it doesn't have to have to same name as what the function calls it. In this next example, you're defining a variable called `planet` and passing it to the `say_hello()` function. Once inside the function, the value ('Mars' in this case) goes into the function's `name` variable because the function was defined as `say_hello(name)`. Your `planet` variable still holds the value 'Mars' as well. But the scope of the `name` variable is the function-- `name` isn't available for use anywhere but inside the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "planet = 'Mars'\n",
    "say_hello(planet)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parameter variables for a function are kept separate from variables in other functions or in the main program. This way you don't have to worry about overwriting a variable that was set elsewhere. For example, you can set a `name` variable in your main code, but if your function has a `name` parameter it doesn't overwrite the one in the main code. Instead, the string that you passed in is used and your variable retains its value. Essentially you have two variables called `name`-- one inside the function and one out, both with different scopes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set a name variable outside the function.\n",
    "name = 'Tami'\n",
    "\n",
    "# When the function is called, its name variable is set to Janet.\n",
    "say_hello(name='Janet')\n",
    "\n",
    "# The name variable outside the function is still set to Tami.\n",
    "print(f'The original name variable is still set to {name}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition, variables created inside of a function aren't available outside of it. This example changes the `say_hello()` function so that it wants a first name and a last name as parameters and then uses them to create a `full_name` variable inside the function. Then it uses the `full_name` variable to print the hello message."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def say_hello(first_name, last_name):\n",
    "    full_name = f'{first_name} {last_name}'\n",
    "    print(f'Hello {full_name}!')\n",
    "    \n",
    "say_hello('Mike', 'Wilson')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But you can't access the `full_name` variable outside of the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "full_name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You could even create a new variable called `name` inside the function and it wouldn't overwrite the `name` variable outside of the function. Let's change the function so that `full_name` is called `name` instead and then test things out."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change the function so the internal variable is called name.\n",
    "def say_hello(first_name, last_name):\n",
    "    name = f'{first_name} {last_name}'\n",
    "    print(f'Hello {name}!')\n",
    "    \n",
    "    \n",
    "# The name variable outside the function is still set to Tami (from earlier).\n",
    "print(f'Original name is {name}.')\n",
    "\n",
    "# Call the say_hello function, which creates its own name variable that isn't a parameter.\n",
    "say_hello('Luke', 'Anderson')\n",
    "\n",
    "# And the name variable outside the function is still set to Tami (from earlier).\n",
    "print(f'Original name is still {name}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Global variables\n",
    "\n",
    "You can, however, create variables in your  main code that are available inside of a function. These are called *global* variables, and using them isn't the greatest coding practice (and isn't nearly so easy to do in a lot of other languages). I'm going to show it to you so that you know how it works and it'll be easier to figure out what's going on if you accidentally do it, but you should stay away from it in most cases because it can be **VERY dangerous** and mess up your data if you don't understand exactly what you're doing.\n",
    "\n",
    "Let's change the `say_hello()` function to use a global variable called `last_name` instead of taking it as a parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def say_hello(name):\n",
    "    print(f'Hello {name} {last_name}!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now if you call the function without setting the `last_name` variable anywhere, you'll get an error."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "say_hello('Rob')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But it'll work if you do set a `last_name` variable before calling the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "last_name = 'Thompson'\n",
    "say_hello('Rob')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use the `last_name` variable inside the function, and even change its value inside the function, but its value outside the function won't change. For example, let's try to change the last name inside the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def say_hello(name):\n",
    "    last_name = 'Smith'\n",
    "    print(f'Hello {name} {last_name}!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now if you call `say_hello()`, the global `last_name` variable is still set to 'Thompson', not 'Smith', although it got set to 'Smith' inside the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Global last_name is Thompson.\n",
    "print(f'Global last_name is {last_name}')\n",
    "\n",
    "# last_name is changed to Smith inside the function.\n",
    "say_hello('John')\n",
    "\n",
    "# Global last_name is still Thompson.\n",
    "print(f'Global last_name is still {last_name}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This behavior keeps you from accidentally changing something when it comes to simple data types like strings, but it *is* possible to change more complex types (like lists), whether you mean to or not (which is why I stressed earlier that this is a **VERY dangerous** practice). In general, it's best to avoid using global variables. \n",
    "\n",
    "So why did I show them to you? Because you'll probably create them accidentally at some point and you need to know what you did. Or maybe you'll see code that uses them. You might even see code that uses the `global` keyword inside a function. This allows you to change the value of a global variable inside the function, but it's usually really bad code design to do something like that, so I'm not even going to show you how to do it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Returning values\n",
    "\n",
    "Your function can also return a value to the caller. Just use the `return` keyword. This example takes two parameters and then returns their sum."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add(n1, n2):\n",
    "    return n1 + n2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the user can call the function and put the result into a variable, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "val = add(2, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now you can print out the value that was returned by the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "val"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you don't capture the result in a variable or explicitly  do something else with it, it'll be printed out if you're using an interactive command line or it's the last line in a notebook cell. Otherwise the result will just disappear into cyberspace."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "add(3, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Actually, all functions return a value. If nothing is specified with a `return` statement, then the function returns `None`, which is the Python version of null or nothing. For example, this prints the Hello message from inside the function, then the function returns `None` to the calling code and it's stored in the `val` variable. When `val` is printed, it prints `None`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "val = say_hello('Mary')\n",
    "print(val)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 3\n",
    "\n",
    "Create a new `multiply()` function that takes two numbers as parameters, multiplies them together, and returns the result. **Do not have it print anything out.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "This calls your function and puts the result in a variable called `result`, and then prints out the value. If you created the function correctly, this will print 10 **ONE time**. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "result = multiply(2, 5)\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Default values\n",
    "\n",
    "Sometimes you want to provide a default value for a parameter so the user doesn't necessarily have to provide one. To do that, use a single equal sign and set the parameter equal to a value in the `def` statement. For example, this changes the `add()` function so that by default the `n2` parameter is 2, but it'll use a different value instead if the user supplies one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add(n1, n2=2):\n",
    "    return n1 + n2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This will return the same result as the earlier `add()` function if the user provides the same inputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "add(2, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But if the user only provides one number, the function just adds 2 to it because `n2` is set to 2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "add(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All parameters with default values must be defined **after** the parameters without default values, so this won't work:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add(n1=1, n2):\n",
    "    return n1 + n2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 4\n",
    "\n",
    "Create a new version of your `multiply()` function that provides a default value of 1 for the first parameter and 10 for the second parameter. Call the first parameter `n`, and the second parameter `m`. Have your function return the product of these two numbers. Again, **do not print anything out**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "Now if you call your function without parameters, it should return 10:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "multiply()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "If you call it with two parameters, then it should return their product (12, in this case):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "multiply(3, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "If you call your function with one parameter, that's used as the first parameter and the default value of the second parameter is used. So passing 5 will result in 5 * 10 = 50."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "multiply(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "Just like with the geoprocessing tools, you can specify a parameter name instead of relying on their order. For example, say you wanted to call your `multiply()` function using only the second (`m`) parameter, so in this case you'd get 1 * 2 = 2:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "multiply(m=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Indentation\n",
    "\n",
    "Let's quickly look at what happens if you mess up your indentation. Try writing a function that converts grams to pounds and prints out a message."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def grams_to_lbs(grams):\n",
    "    lbs = grams * 0.0022046\n",
    "     print(f'{grams} grams is equal to {lbs} pounds')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Uh oh. Python thinks there's an unexpected indent. Okay, so you try to make it happy by unindenting that line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def grams_to_lbs(grams):\n",
    "    lbs = grams * 0.0022046\n",
    "print(f'{grams} grams is equal to {lbs} pounds')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now what's the problem? Because that line isn't indented at all, Python thinks it's not part of the function. But the `grams` and `lbs` variables only exist inside the function (remember the section on scope?), so Python doesn't know what they are anymore. Let's indent again and see what happens. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def grams_to_lbs(grams):\n",
    "    lbs = grams * 0.0022046\n",
    "    print(f'{grams} grams is equal to {lbs} pounds')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Much better. As you probably figured out, the problem with the first example was that there was an extra space in the second line's indentation. Now that you've got that fixed, everybody's happy and the function works as expected:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grams_to_lbs(12)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you've seen how to create them, it should be apparent that it would be really easy to convert one of your homework notebooks to a function. Instead of setting variables at the top of the notebook, convert them to function parameters on your `def` line, indent your code, add a `return` statement if needed, and you're done! (All of the code would need to be in one cell, however.)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "281.641px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
