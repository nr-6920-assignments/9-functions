# Functions

This week you'll learn how to write your own functions and create modules. This doesn't have anything to do with GIS, but is really important information to know. You'll still use arcpy for the homework problems when you create your own function and module.

See the [homework description](homework.md).
