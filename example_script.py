import random

# Create a list of 10 random numbers.
numbers = [random.random() for i in range(10)]
print('Numbers:', numbers)

# Print a blank line.
print()

# Now let's multiply each one by 2.
for n in numbers:
    print(f'2 * {n} = {2 * n}')