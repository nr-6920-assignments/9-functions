import random
import sys


# Define the function with a default value for count.
def print_random(count=10):

    # Create a list of 10 random numbers.
    numbers = [random.random() for i in range(count)]
    print('Numbers:', numbers)

    # Print a blank line.
    print()

    # Now let's multiply each one by 2.
    for n in numbers:
        print(f'2 * {n} = {2 * n}')


# Check if running as a script.
if __name__ == '__main__':
    
    # If the user provided a parameter when running the script then 
    # sys.argv will have more than one thing in it. If they didn't, 
    # then sys.argv will only contain the filename.
    if len(sys.argv) > 1:
        
        # The user did provide a parameter, so get it and pass it
        # to the function.
        count = int(sys.argv[1])
        print_random(count)
        
    else:
        
        # The user didn't provide a paremter, so call the function
        # and let it use its default value.
        print_random()