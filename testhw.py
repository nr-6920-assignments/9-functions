import os
from tests.testtools import BaseTester, Runner, STREAM

__unittest = True

def test_results(problem):
    if problem == 1:
        Runner.test_output(problem, Problem1R)
    elif problem == 2:
        Runner.test_output(problem, Problem2R)
    elif problem == 3:
        Runner.test_output(3, Problem3NBa)

def test_notebook(problem):
    if problem == 1:
        Runner.run_problem(problem, [Problem1a, Problem1b], True, ['test_notebook'])
    elif problem == 2:
        Runner.run_problem(2, [Problem2a, Problem2b], True, ['test_notebook'])
    elif problem == '3F':
        Runner.run_problem(3, [Problem3Fa, Problem3Fb], True, ['test_function'])
    elif problem == '3NB':
        Runner.run_problem(3, [Problem3NBa, Problem3NBb], True, ['test_notebook'])


class ProblemTester(BaseTester):
    problem = 0
    args = {}
    station_filename = 'WeatherStations_test.shp'
    network = ''
    result_filename = ''
    expected_filename = ''

    def setUp(self):
        super().setUp()
        self.args = dict(
            station_filename=os.path.join(self.data_folder, self.station_filename),
            output_filename=os.path.join(self.data_folder, self.result_filename),
            network=self.network,
        )
        self.expected_path = os.path.join(self.test_folder, 'data', self.expected_filename)
        self.result_path = os.path.join(self.data_folder, self.result_filename)

    def test_rows(self):
        """Test number of output rows"""
        self.notebook_ran()
        self.result_exists()
        self._test_rows(self.expected_path, self.result_path)

    def test_columns(self):
        """Test columns"""
        self.notebook_ran()
        self.result_exists()
        self._test_columns(self.expected_path, self.result_path)

    def test_data(self):
        """Test data values"""
        self.notebook_ran()
        self.result_exists()
        self._test_data(self.expected_path, self.result_path)

    def _test_rows(self, expected_path, result_path):
        result_df, expected_df = self._get_data(expected_path, result_path)
        result_n, expected_n = len(result_df), len(expected_df)
        self.assertEqual(result_n, expected_n, f'Output has {result_n} features instead of {expected_n}')

    def _test_columns(self, expected_path, result_path):
        result_df, expected_df = self._get_data(expected_path, result_path)
        result_cols, expected_cols = result_df.columns, expected_df.columns
        self.assertEqual(set(result_cols), set(expected_cols), f'Output columns are {result_cols} instead of {expected_cols}')

    def _test_data(self, expected_path, result_path):
        result_df, expected_df = self._get_data(expected_path, result_path)
        self.assertTrue(result_df.equals(expected_df), f'Output data appears to be incorrect')

    def _get_data(self, expected_path, result_path):
        import pandas as pd
        import arcpy
        with arcpy.da.SearchCursor(result_path, '*') as rows:
            result_df = pd.DataFrame(data=list(rows), columns=rows.fields).round(2).rename(columns=str.lower)
        result_df = result_df.drop(['fid', 'shape'], axis='columns')
        dtypes = {k: str if v == 'object' else v for k, v in result_df.dtypes.items()}
        expected_df = pd.read_csv(expected_path, dtype=dtypes).round(2).rename(columns=str.lower)
        return result_df.set_index('stn_id_'), expected_df.set_index('stn_id_')


class Problem1a(ProblemTester):
    problem = 1
    suffix = 'a'
    network = 'snotel'
    result_filename = 'snotel_test.shp'
    expected_filename = 'snotel.csv'


class Problem1b(ProblemTester):
    problem = 1
    suffix = 'b'
    network = 'gse'
    result_filename = 'gse_test.shp'
    expected_filename = 'gse.csv'


class Problem1R(Problem1a):
    result_filename = 'snotel.shp'


class Problem2a(Problem1b):
    problem = 2
    suffix = 'a'


class Problem2b(Problem1a):
    problem = 2
    suffix = 'b'


class Problem2R(Problem2a):
    result_filename = 'gse.shp'


class Problem3NB(ProblemTester):
    problem = 3
    networks = []
    result_folder = 'test'

    def setUp(self):
        super().setUp()
        self.args = dict(
            folder=self.data_folder,
            station_filename=self.station_filename,
            networks=self.networks,
        )

    def test_rows(self):
        """Test number of output rows"""
        self.notebook_ran()
        for network in self.networks:
            result_path = self._get_result(network)
            self.result_exists(result_path)
            self._test_rows(self._get_expected(network), result_path)

    def test_columns(self):
        """Test columns"""
        self.notebook_ran()
        for network in self.networks:
            result_path = self._get_result(network)
            self.result_exists(result_path)
            self._test_columns(self._get_expected(network), result_path)

    def test_data(self):
        """Test data values"""
        self.notebook_ran()
        for network in self.networks:
            result_path = self._get_result(network)
            self.result_exists(result_path)
            self._test_data(self._get_expected(network), result_path)

    def result_exists(self, result_path):
        if not os.path.exists(result_path):
            self.skipTest(f'Output file {result_path.replace(self.repo_folder, "")} not found')

    def _get_expected(self, network):
        return os.path.join(self.test_folder, 'data', network + '.csv')

    def _get_result(self, network):
        return os.path.join(self.data_folder, network + '.shp')


class Problem3NBa(Problem3NB):
    suffix = 'a'
    networks = ['COOP', 'NEXRAD', 'RAWS', 'UPR']


class Problem3NBb(Problem3NB):
    suffix = 'b'
    networks = ['BLM']


class Problem3F(ProblemTester):
    problem = 3
    result_filename = 'hads.shp'
    expected_filename = 'hads.csv'
    network = 'HADS'
    result_count = 141

    def _call_function(self, station_filename, output_filename, network):
        STREAM.write(f'\n  station_filename = {station_filename}')
        STREAM.write(f'\n  output_filename = {output_filename}')
        STREAM.write(f'\n  network = {network}\n')
        if not os.path.exists(os.path.join(self.repo_folder, 'problem3.py')):
            self.skipTest(f'Module problem3.py not found')
        self.copy_data()
        import sys
        sys.path.append(self.repo_folder)
        import problem3
        result = problem3.extract_stations(station_filename, output_filename, network)
        self.assertIsInstance(result, int, f'Function returned an object of type {type(result)} instead of integer')
        self.assertEqual(result, self.result_count, f'Function returned {result} instead of {self.result_count}')


class Problem3Fa(Problem3F):
    suffix = 'f'

    def test_function(self):
        """Test function"""
        self._call_function(
            os.path.join(self.data_folder, self.station_filename),
            os.path.join(self.data_folder, self.network + '.shp'),
            self.network,
        )


class Problem3Fb(Problem3F):
    suffix = 'g'

    def test_function(self):
        """Test function"""
        import arcpy
        arcpy.env.workspace = self.data_folder
        self._call_function(self.station_filename, self.network + '.shp', self.network)


if __name__ == '__main__':
    test_notebook(1)
    test_notebook(2)
    test_notebook('3F')
    test_notebook('3NB')
