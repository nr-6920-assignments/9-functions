def hello(name='World'):
    print(f'Hello {name}!')

def add(n1, n2, n3):
    return n1 + n2 + n3

print(f'__name__ = {__name__}')
if __name__ == '__main__':
    print('Running as a script!')
    hello('Dolly')
