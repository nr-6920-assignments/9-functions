# Functions homework

*Due Monday, March 29, at midnight*

## Notebook comprehension questions

*Worth 11 points*

- 1-scripts (3 points)
- 2-functions (5 points)
- 3-modules (2 points)
- 4-classes (1 point)


## Script problems

*Worth 10 points each*

## To get full credit for this week's problems

1. Name your notebook exactly how I tell you, with no spaces.
2. Put the variables I give you in the first code cell. Don't put anything else in that cell.
3. If you need to use the value of one of those variables, use the variable instead of typing out the value. Your notebook should not have those values anywhere except in the first cell.
4. Include explanations of what your code does.
5. Include all plots or anything else that I ask for.
6. Make sure your notebooks runs correctly after the kernel is restarted. You can use the run-tests notebook to help check this.

For these problems I'm going to give you a shapefile containing weather stations from around the state of Utah. Each weather station belongs to a certain network, such as NWS/FAA (Federal Aviation Administration) or SNOTEL (Snow Telemetry). Pretend that you need to be able to extract all of the weather stations in a certain network and put them in their own shapefile. This is easy to do manually if you only need to do one, but what if you need to do it for ten or more different networks? That gets old really fast. So you're going to (eventually) write a function to do it. Problem 1 will be creating a notebook to do it, since that's what you're used to doing. Problem 2 will be converting it to a function inside a notebook, and problem 3 will be putting the function in a module and then using it in a notebook. 

## Turn in
1. problem1.ipynb
2. problem2.ipynb
3. problem3.ipynb
4. problem3.py

## Problem 1

Create a notebook called `problem1`. Set up three variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
station_filename = r'D:\classes\NR6920\Assignments\9-functions\data\WeatherStations.shp'
output_filename = r'D:\classes\NR6920\Assignments\9-functions\data\snotel.shp'
network = 'snotel'
```

There shouldn't be anything else in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

1. Have your notebook find all of the weather stations in the `station_filename` shapefile that belong to the provided `network`, and save the results into a new shapefile with `output_filename`. 
2. The field in the shapefile that contains the network name is called "NETWORK_" (yeah, it had the underscore when I downloaded the dataset and I didn't bother to copy the field to one with a better name). **All of the network names in the shapefile are uppercase, but make your code work even if the provided `network` is lowercase** (what's a simple way to convert the `network` variable so that its value would match the uppercase names in the shapefile?). DO NOT change the value of `network` in that first cell. It needs to be lowercase there.
3. Use `GetCount()` to print out the number of features in your output shapefile. 
4. As usual, do not include the filenames or the network name anywhere else in your notebook.
5. You're using full paths to the files, so don't set a workspace. See the hint below if you need to be able to get the folder that the file will be created in.

**There are *lots* of ways to solve this problem. I don't care which method you use, as long as it uses `arcpy`.**

You will probably need to split the `output_filename` up into a folder and filename in order to make a particular arcpy tool happy. Here's how you can do it so that it'll work if `output_filename` is a full path or if the workspace has been set and `output_filename` is a filename with no path:

```python
import os

# Split output_filename into folder and filename
folder, filename = os.path.split(output_filename)

# If output_filename isn't a full path, then the folder variable will be 
# a blank string. In that case, set folder to the current workspace.
if folder == '':
    folder = arcpy.env.workspace
```

Now if you need the path split up, you can use the `folder` and `filename` variables instead of the `output_filename` variable.

If you want to see a trick, you can replace the `if` block with a single line of code instead. When Python evaluates an `or` statement it stops after the first thing it finds that evaluates to `True` and it returns that value. If `folder` isn't a blank string, then it will evaluate to `True` and it will be returned from the `or` and put back into `folder` (so nothing changed). But if `folder` is an empty string, then Python evaluates the contents of `arcpy.env.workspace` and puts that into the `folder` variable.

```python
folder, filename = os.path.split(output_filename)
folder = folder or arcpy.env.workspace
```

### Results

The output `snotel` shapefile should have all the same attributes and the same spatial reference as WeatherStations.shp, but will only have 88 features in it instead of over 1500. You used `GetCount()` to verify the 88 features, but it's up to you to verify the rest. You can open the shapefiles up in ArcGIS and compare, or you can put some code in your notebook to do it.

## Problem 2

Create a notebook called `problem2`. Set up three variables in the very first code cell, with these names and values (except change the path to the data folder on **your** computer:

```python
station_filename = r'D:\classes\NR6920\Assignments\9-functions\data\WeatherStations.shp'
output_filename = r'D:\classes\NR6920\Assignments\9-functions\data\gse.shp'
network = 'gse'
```

There shouldn't be anything else in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

1. Import whatever modules you needed for problem 1.
2. Create a function called `extract_stations` using your code from problem 1 (except any `import` statements). Make sure you call it `extract_stations` so that my test code can run it. 
3. Make your function take three parameters, in this order. You can call them whatever you want, as long as the names are descriptive, but the order is important because it needs to match my test code. *(Hint: If you give them the same names as the variables in the problem 1 notebook, then you can copy/paste your code without changing any of the variable names.)*
    1. station filename
    2. output filename
    3. network name
4. In your function, still use `GetCount()` to get the number of features in the new shapefile, but **don't print it out inside the function**. Instead, return the count from the function. `GetCount()` gives you a [Result](https://pro.arcgis.com/en/pro-app/arcpy/classes/result.htm) object. Don't return this object, but instead use `getOutput()` to get the numeric count out of the result and return that from your function. (If the documentation isn't enough, Result objects are discussed briefly at the end of the Running geoprocessing tools section of the arcpy-intro notebook from the arcpy-intro assignment.) `getOutput()` will give you a string, so convert it to an integer **before** returning it. It might be useful to test this outside of your function first. For example, call `GetCount()` and put the result into a variable. Print that out. Then use `getOutput(0)` on it and store *that* in a variable. That should be the number, but it'll be a string. Then use `int()` to convert it to a number. That's what you'll want to return from the function.
5. There are very few cases where you'd want to put the `import` statements inside a function, and this isn't one of them, so make sure that all of your `import` statements are *before* your function definition.
6. Don't set `arcpy.env.overwriteOutput` in your function (you can put it somewhere else in your notebook if you need to, though). Things like this should be the responsibility of whoever is using your function, because they might *not* want to overwrite any existing file. If you put it in your function, then you take away their control. 
7. Now add another cell to your notebook after the one that creates the function. In this cell, call your function using the `station_filename`, `output_filename`, and `network` variables from the first cell. Don't type out the filenames or network name-- use the variables. Store the feature count returned from the function in a variable. 
8. Print out the contents of the variable from step 7 and then use `type(your_variable_name)` to make sure it's an integer. It should print `<class 'int'>` if you use `print()` or just `int` if you don't.

### Results

The output `gse` shapefile should have all the same attributes and the same spatial reference as WeatherStations.shp, but will only have 19 features in it instead of over 1500. Your function should've used `GetCount()` and returned the number of features as an integer, but it's up to you to verify the rest. You can open the shapefiles up in ArcGIS and compare, or you can put some code in your notebook to do it.

## Problem 3

Create a Python script called `problem3.py` and copy your `import` statements and `extract_stations` function from problem 2 into it. Do not include anything else (for example, don't include anything to set the workspace or overwriteOutput). Make sure you save your file.

Now create a notebook called `problem3`. Set up three variables in the very first code cell, with this name and value (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\NR6920\Assignments\9-functions\data'
station_filename = 'WeatherStations.shp'
networks = ['COOP', 'NEXRAD', 'RAWS', 'UPR']
```

There shouldn't be anything else in that first cell, and these values should not appear anywhere else in your notebook. Always use the variables that you set here.

Put code in your notebook that creates shapefiles for all of the networks in the `networks` list. 

This will mimic how you might use your module in real life. It's going to assume that you want everything in your `data` folder, so set your workspace using the `folder` variable. This is actually the only reason you even need to import arcpy into the notebook, since the rest of the arcpy code is in your module! 

If you didn't want to set the workspace (because you didn't want to import arcpy or if you wanted to put the files in different folders), then you'd need to provide full paths to all of the files. If you'd like to try it this way instead of importing arcpy, then use `os.path.join()` to create all of your filenames. For example, you could create the station filename with `os.path.join(folder, station_filename)`.

1. You're going to use your function that's in the `problem3.py` file, so import that file as a module. **Do not put the function code in your notebook!** Once you've done that you can set up your code and use your `extract_stations()` functions just like you've been doing all semester with arcpy. If you set your arcpy workspace in your notebook, it will still be in effect when you call your function.
2. Iterate over the list of networks with a loop. Inside your loop:
    1. Create an output filename based on the network name (for example, `coop.shp`). You can create an appropriate output filename by joining the network name and '.shp' (capitalization isn't important). Create these filenames dynamically (for example, do not actually type out `coop.shp` anywhere). You can use `+` or *f*-strings or `format()` to do this. 
    2. Call the `extract_stations` function for the current network. The station filename for this will always be your `station_filename` variable. 
    3. Right after it calls the function using the network, have it print out the network name and the number of features in the new shapefile (don't include `GetCount()` here-- use the value returned from the function).

##### Results

This should create four more shapefiles and print out something like this:

```
coop.shp 475
nexrad.shp 2
raws.shp 74
upr.shp 24
```

**Don't forget to turn in both your notebook and your Python script for this problem!**
